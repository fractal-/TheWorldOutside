
set(ENABLED_GEMS
    O3DE-Jam
    Atom
    AudioSystem

    CameraFramework
    DebugDraw
    EditorPythonBindings
    EMotionFX
    GameState
    ImGui
    LandscapeCanvas
    LyShine
    
    PhysX
    PrimitiveAssets
    SaveData
    ScriptCanvasPhysics
    ScriptEvents
    StartingPointInput
    TextureAtlas
    WhiteBox
    Terrain
)
