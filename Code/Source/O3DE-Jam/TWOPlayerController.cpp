#include <AzCore/Module/Module.h>
#include <O3DE-Jam/TWOPlayerController.h>
#include <AzCore/Serialization/SerializeContext.h>
#include <AzCore/Serialization/EditContext.h>

void TWOPlayerController::Init()
{

}

void TWOPlayerController::Activate()
{
// AZ::EBusHandlerPolicy
}

void TWOPlayerController::Deactivate()
{
    
}

void TWOPlayerController::SetFlashlightOn(bool bOn)
{
    bLightOn = bOn;
    if (bLightOn)
    {

    }
}

void TWOPlayerController::Reflect(AZ::ReflectContext* context)
{
    AZ::SerializeContext* serialize = azrtti_cast<AZ::SerializeContext*>(context);
    if (serialize)
    {
        // Reflect the class fields that you want to serialize.
        // In this example, m_runtimeStateNoSerialize is not reflected for serialization.
        // Base classes with serialized data should be listed as additional template
        // arguments to the Class< T, ... >() function.
        serialize->Class<TWOPlayerController, AZ::Component>()
            ->Version(1)
            ->Field("LightIntensity", &TWOPlayerController::LightIntensity)
            ->Field("LightOn", &TWOPlayerController::bLightOn)
            ->Field("LightEntity", &TWOPlayerController::LightEntity)
            ;

        AZ::EditContext* edit = serialize->GetEditContext();
        if (edit)
        {
            edit->Class<TWOPlayerController>("TWOPlayerController", "PlayerController for TheWorldOutside")
                ->ClassElement(AZ::Edit::ClassElements::EditorData, "")
                    ->Attribute(AZ::Edit::Attributes::AppearsInAddComponentMenu, AZ_CRC("Game"))
                ->DataElement(AZ::Edit::UIHandlers::Default, &TWOPlayerController::LightIntensity, "Light Intensity", "The intensity of the light in lumen")
                ->DataElement(AZ::Edit::UIHandlers::Default, &TWOPlayerController::bLightOn, "Light On", "If the light is on or not")
                ->DataElement(AZ::Edit::UIHandlers::Default, &TWOPlayerController::LightEntity, "Light Entity", "The entity bearing the light component")
                ;
        }
    }
}

//void TWOPlayerController::GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType&)
//{
//
//}
//
//void TWOPlayerController::GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType&)
//{
//
//}
//
//void TWOPlayerController::GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType&)
//{
//
//}
