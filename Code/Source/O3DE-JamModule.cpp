
#include <AzCore/Memory/SystemAllocator.h>
#include <AzCore/Module/Module.h>

#include "O3DE-JamSystemComponent.h"
#include "O3DE-Jam/TWOPlayerController.h"

namespace O3DE_Jam
{
    class O3DE_JamModule
        : public AZ::Module
    {
    public:
        AZ_RTTI(O3DE_JamModule, "{47b3a424-3f8c-4ed1-9b8e-daa994ac815f}", AZ::Module);
        AZ_CLASS_ALLOCATOR(O3DE_JamModule, AZ::SystemAllocator, 0);

        O3DE_JamModule()
            : AZ::Module()
        {
            // Push results of [MyComponent]::CreateDescriptor() into m_descriptors here.
            m_descriptors.insert(m_descriptors.end(), {
                O3DE_JamSystemComponent::CreateDescriptor(),
                TWOPlayerController::CreateDescriptor(),
            });
        }

        /**
         * Add required SystemComponents to the SystemEntity.
         */
        AZ::ComponentTypeList GetRequiredSystemComponents() const override
        {
            return AZ::ComponentTypeList{
                azrtti_typeid<O3DE_JamSystemComponent>(),
            };
        }
    };
}// namespace O3DE_Jam

AZ_DECLARE_MODULE_CLASS(Gem_O3DE_Jam, O3DE_Jam::O3DE_JamModule)
