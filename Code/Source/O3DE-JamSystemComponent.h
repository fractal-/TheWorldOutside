
#pragma once

#include <AzCore/Component/Component.h>

#include <O3DE-Jam/O3DE-JamBus.h>

namespace O3DE_Jam
{
    class O3DE_JamSystemComponent
        : public AZ::Component
        , protected O3DE_JamRequestBus::Handler
    {
    public:
        AZ_COMPONENT(O3DE_JamSystemComponent, "{33b49b4b-b36c-4913-8d78-abc78b68443a}");

        static void Reflect(AZ::ReflectContext* context);

        static void GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided);
        static void GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible);
        static void GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required);
        static void GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent);

        O3DE_JamSystemComponent();
        ~O3DE_JamSystemComponent();

    protected:
        ////////////////////////////////////////////////////////////////////////
        // O3DE_JamRequestBus interface implementation

        ////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////////////////////////////////////
        // AZ::Component interface implementation
        void Init() override;
        void Activate() override;
        void Deactivate() override;
        ////////////////////////////////////////////////////////////////////////
    };
}
