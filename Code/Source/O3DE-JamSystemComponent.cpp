
#include <AzCore/Serialization/SerializeContext.h>
#include <AzCore/Serialization/EditContext.h>
#include <AzCore/Serialization/EditContextConstants.inl>

#include "O3DE-JamSystemComponent.h"

namespace O3DE_Jam
{
    void O3DE_JamSystemComponent::Reflect(AZ::ReflectContext* context)
    {
        if (AZ::SerializeContext* serialize = azrtti_cast<AZ::SerializeContext*>(context))
        {
            serialize->Class<O3DE_JamSystemComponent, AZ::Component>()
                ->Version(0)
                ;

            if (AZ::EditContext* ec = serialize->GetEditContext())
            {
                ec->Class<O3DE_JamSystemComponent>("O3DE_Jam", "[Description of functionality provided by this System Component]")
                    ->ClassElement(AZ::Edit::ClassElements::EditorData, "")
                        ->Attribute(AZ::Edit::Attributes::AppearsInAddComponentMenu, AZ_CRC("System"))
                        ->Attribute(AZ::Edit::Attributes::AutoExpand, true)
                    ;
            }
        }
    }

    void O3DE_JamSystemComponent::GetProvidedServices(AZ::ComponentDescriptor::DependencyArrayType& provided)
    {
        provided.push_back(AZ_CRC("O3DE_JamService"));
    }

    void O3DE_JamSystemComponent::GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible)
    {
        incompatible.push_back(AZ_CRC("O3DE_JamService"));
    }

    void O3DE_JamSystemComponent::GetRequiredServices([[maybe_unused]] AZ::ComponentDescriptor::DependencyArrayType& required)
    {
    }

    void O3DE_JamSystemComponent::GetDependentServices([[maybe_unused]] AZ::ComponentDescriptor::DependencyArrayType& dependent)
    {
    }
    
    O3DE_JamSystemComponent::O3DE_JamSystemComponent()
    {
        if (O3DE_JamInterface::Get() == nullptr)
        {
            O3DE_JamInterface::Register(this);
        }
    }

    O3DE_JamSystemComponent::~O3DE_JamSystemComponent()
    {
        if (O3DE_JamInterface::Get() == this)
        {
            O3DE_JamInterface::Unregister(this);
        }
    }

    void O3DE_JamSystemComponent::Init()
    {
    }

    void O3DE_JamSystemComponent::Activate()
    {
        O3DE_JamRequestBus::Handler::BusConnect();
    }

    void O3DE_JamSystemComponent::Deactivate()
    {
        O3DE_JamRequestBus::Handler::BusDisconnect();
    }
}
