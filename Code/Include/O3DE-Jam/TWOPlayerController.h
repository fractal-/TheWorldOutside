#pragma once

#include <AzCore/Component/Component.h>

class TWOPlayerController : public AZ::Component
{
public:
    AZ_COMPONENT(TWOPlayerController, "{0DC02E4C-1A47-443E-982A-4684126D8E7B}", AZ::Component);

    float LightIntensity = 100.0f;
    bool bLightOn = false;
    AZ::EntityId LightEntity;

    void Init() override;
    void Activate() override;
    void Deactivate() override;

    void SetFlashlightOn(bool bOn);

    static void Reflect(AZ::ReflectContext* provided);
    //static void GetDependentServices(AZ::ComponentDescriptor::DependencyArrayType& dependent);
    //static void GetRequiredServices(AZ::ComponentDescriptor::DependencyArrayType& required);
    //static void GetIncompatibleServices(AZ::ComponentDescriptor::DependencyArrayType& incompatible);
};
