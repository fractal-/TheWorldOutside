
#pragma once

#include <AzCore/EBus/EBus.h>
#include <AzCore/Interface/Interface.h>

namespace O3DE_Jam
{
    class O3DE_JamRequests
    {
    public:
        AZ_RTTI(O3DE_JamRequests, "{05f00a48-451c-40fb-8a8f-f2b203c83ff5}");
        virtual ~O3DE_JamRequests() = default;
        // Put your public methods here
    };
    
    class O3DE_JamBusTraits
        : public AZ::EBusTraits
    {
    public:
        //////////////////////////////////////////////////////////////////////////
        // EBusTraits overrides
        static constexpr AZ::EBusHandlerPolicy HandlerPolicy = AZ::EBusHandlerPolicy::Single;
        static constexpr AZ::EBusAddressPolicy AddressPolicy = AZ::EBusAddressPolicy::Single;
        //////////////////////////////////////////////////////////////////////////
    };

    using O3DE_JamRequestBus = AZ::EBus<O3DE_JamRequests, O3DE_JamBusTraits>;
    using O3DE_JamInterface = AZ::Interface<O3DE_JamRequests>;

} // namespace O3DE_Jam
