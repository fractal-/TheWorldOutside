local PlayerInput =
{
	Properties = 
	{
		Flashlight = {
			Intensity = { default = 100.0, suffix = " lumen" },
			On = false,
			LightEntity = { default = EntityId() },
		},
		CameraEntity = { default = EntityId() },
	}
}

function PlayerInput:SetFlashlightOn(On)
	self.Properties.Flashlight.On = On
	if (self.Properties.Flashlight.On) then
		AreaLightRequestBus.Event.SetIntensity(self.Properties.Flashlight.LightEntity, self.Properties.Flashlight.Intensity)
	else
		AreaLightRequestBus.Event.SetIntensity(self.Properties.Flashlight.LightEntity, 0.0)
	end
end

function PlayerInput:OnActivate()
	self.inputChannelNotificationBus = InputChannelNotificationBus.Connect(self)
	self:SetFlashlightOn(self.Properties.Flashlight.On)
	-- self.gameplayBus = GameplayNotificationBus.Connect(self, gameplayBusId)
	self.tickBus = TickBus.Connect(self)
	physicsSystem = GetPhysicsSystem()
	sceneHandle = physicsSystem:GetSceneHandle(DefaultPhysicsSceneName)
	self.scene = physicsSystem:GetScene(sceneHandle)
	self.raycastTimer = 0.0
end

function PlayerInput:OnTick(DeltaTime)
	self.raycastTimer = self.raycastTimer + DeltaTime
	if (self.raycastTimer < 0.03) then return end
	self.raycastTimer = 0.0

	-- local TM = TransformBus.Event.GetWorldTM(self.Properties.CameraEntity)
	request = RayCastRequest()
	request.Start = TransformBus.Event.GetWorldTranslation(self.Properties.CameraEntity)
	request.Direction = TransformBus.Event.GetWorldRotation(self.Properties.CameraEntity)
	request.Distance = 10.0
	request.ReportMultipleHits = false
	hits = self.scene:QueryScene(request)
	numHits = hits.HitArray:Size()
	if (numHits > 0) then
		local hitEntity = hits.HitArray[0].EntityId
		-- Debug.Log(tostring(hitEntity))
		GameplayNotificationBus.Event.OnEventBegin(GameplayNotificationId(hitEntity, "OnInteract", "EntityId"), self.EntityId)
		-- Debug.Log("hit !")
	end
end

function PlayerInput:OnDeactivate()
	if  (self.inputChannelNotificationBus) then
		self.inputChannelNotificationBus:Disconnect()
	end
	if (self.tickBus) then 
		self.tickBus:Disconnect()
	end
end

function PlayerInput:ToggleFlashlight()
	Debug.Log("toggle flash !")
	self:SetFlashlightOn(not self.Properties.Flashlight.On)
end

function PlayerInput:Interact()
	Debug.Log("Interact !")
end

function PlayerInput:OnInputChannelEvent(inputChannel)
	if (inputChannel.state ~= 1) then return end -- 1 stands for event "pressed"
	if (inputChannel.channelName == "flashlight") then
		self:ToggleFlashlight()
	elseif (inputChannel.channelName == "interact") then
		self:Interact()
	end
end

return PlayerInput
