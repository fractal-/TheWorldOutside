local Interactable = 
{
	Properties = 
	{
	}
}

function Interactable:OnActivate()
	local gameplayBusId = GameplayNotificationId(self.entityId, "OnInteract", "EntityId")
    self.gameplayBus = GameplayNotificationBus.Connect(self, gameplayBusId)
end

function Interactable:OnEventBegin(fromEntity)
	Debug.Log(tostring(fromEntity) .. " is trying to interact !")
end

function Interactable:OnDeactivate()
    self.gameplayBus:Disconnect()
end

return Interactable