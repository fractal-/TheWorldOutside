local PlayerCameraInput =
{
	Properties = 
	{
		MoveSpeed = { default = 3.0, description = "How fast the character moves (in meters per second).", suffix = " m/s" },
		RotationSpeed = { default = 360.0, description = "How fast (in degrees per second) the character turns.", suffix = " deg/sec"},
		CameraFollowDistance = {default = 5.0, description = "Distance (in meters) from which camera follows character.", suffix = " m"},
		CameraFollowHeight = {default = 1.0, description = "Height (in meters) from which camera follows character.", suffix = " m"},
		CameraLerpSpeed = {default = 5.0, description = "Coefficient for how tightly camera follows character."},
		Camera = { default = EntityId() },
		InitialState = "Idle",
		DebugStateMachine = false,
	}
}

function PlayerCameraInput:OnActivate()
	self.inputChannelNotificationBus = InputChannelNotificationBus.Connect(self);
end

function PlayerCameraInput:OnDeactivate()
	if  (self.inputChannelNotificationBus) then
		self.inputChannelNotificationBus:Disconnect(self);
	end
end

function  PlayerCameraInput:OnInputChannelEvent(inputChannel)
	Debug.Log("-------------------------------------------")
	Debug.Log("OnInputChannelEvent channelName = " .. inputChannel.channelName)
	Debug.Log("OnInputChannelEvent deviceName = " .. inputChannel.deviceName)
	Debug.Log("OnInputChannelEvent state = " .. inputChannel.state)
	Debug.Log("OnInputChannelEvent value = " .. inputChannel.value)
end

return PlayerCameraInput
