The World Outside
=================

This is a submission for the 2021 O3DE Jam themed "The World Outside"


Open source tools used for the project
--------------------------------------

- O3DE for the engine
- CMake for the build system
- Notepad++ for Lua and some text editing
- Visual Studio Code for Lua and README (Markdown editing)
- Emacs in org mode for notes taking
- Blender for 3D modeling
- Krita for texture painting
- Gimp for UI editing
- LMMS for audio production (also tested SuperCollider)
- Audacity for audio manipulation
- Bash for the command line and grep utility for searching through source code
- Git and git extensions for versionning
- Gitlab for Git hosting
- Vim for commiting and peeking at files on the CLI
- Firefox for web browsing
- Freetube for youtube browsing
- Dygma Raise keyboard with open source firmware and configuration app
- (Tried to use linux as the OS but the engine setup is too much of a hassle and takes too much disk space)


Licences
--------

The source code in this repository is under the GPLv2 license.
All 3D and 2D assets are under the Creative Comons license.


How to play
-----------

Compile the project using O3DE guidelines. It was developed using the Developer Preview of the engine.

The game is mostly composed of Script Canvas so that should be pretty simple to get it to compile. To play the final game, just open the "Levels/House" level in the editor and hit play !